<?php

/**
 * CLI Notification.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2020 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace FunZone;

use FunZone\Enum\SoundEnum;

/**
 * Class Sound
 *
 * @package Codeception\Fun
 */
class Sound
{
    /**
     * @var array<string, string>
     */
    private static $commands = [
        'mpg123' => '-q "{{file}}"',
        'ffplay' => '"{{file}}" -nodisp -autoexit',
        'powershell' => '-c (New-Object Media.SoundPlayer "{{file}}").PlaySync();',
    ];

    /**
     * @var bool
     */
    private static $isCurrentlyPlaying = false;

    /**
     * @param \FunZone\Enum\SoundEnum $sound
     * @return bool
     */
    public static function play(SoundEnum $sound): bool
    {
        $soundFile = dirname(__FILE__, 1).DIRECTORY_SEPARATOR.
            'sounds'.DIRECTORY_SEPARATOR.$sound->getValue()
        ;
        return self::playSound($soundFile);
    }

    /**
     * Plays a sound.
     *
     * @param string $sound
     * @return bool
     */
    private static function playSound(string $sound): bool
    {
        $command = self::getAvailableCommand();

        if (null === $command) {
            return false;
        }

        $commandEncoded = str_replace('{{file}}', $sound, self::$commands[$command]);

        /** Makes sure that always only one sound is getting played */
        if (self::$isCurrentlyPlaying) {
            return false;
        }

        self::$isCurrentlyPlaying = true;
        exec($command.' '.$commandEncoded);
        self::$isCurrentlyPlaying = false;

        return true;
    }

    /**
     * Looks for the available command and returns its name.
     *
     * @return string|null
     */
    private static function getAvailableCommand(): ?string
    {
        $commands = array_keys(self::$commands);

        foreach ($commands as $command) {
            if (self::isCommandAvailable($command)) {
                return $command;
            }
        }

        return null;
    }

    /**
     * Returns if a command is available.
     *
     * @param string $name
     * @return bool
     */
    private static function isCommandAvailable(string $name): bool
    {
        exec($name.' -v > /dev/null 2>&1 & echo $!', $version, $exitCode);
        return !empty($version) && 0 === $exitCode;
    }
}
