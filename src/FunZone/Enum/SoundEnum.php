<?php

/**
 * CLI Notification.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2020 Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace FunZone\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class SoundEnum
 *
 * @method static SoundEnum ANGRY()
 * @method static SoundEnum SAD()
 * @method static SoundEnum COOL()
 */
final class SoundEnum extends Enum
{
    private const COOL = 'ok.mp3';
    private const SAD = 'sad.mp3';
    private const ANGRY = 'angry.mp3';
}
