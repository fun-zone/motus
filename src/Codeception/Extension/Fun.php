<?php
namespace Codeception\Extension;

use FunZone\Sound;
use FunZone\Enum\SoundEnum;

class Fun extends \Codeception\Platform\Extension {

    static $events = array('result.print.after' => 'notify');

    function notify($event)
    {
        $result = $event->getResult();

        if ($result->errorCount() > 0) {
            Sound::play(
                SoundEnum::ANGRY()
            );
        } else if ($result->failureCount() > 0 ) {
            Sound::play(
                SoundEnum::SAD()
            );
        } else {
            Sound::play(
                SoundEnum::COOL()
            );
        }
    }

}
