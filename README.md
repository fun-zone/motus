# Installation
### 1. in docker
add `RUN apt-get install mpg123` in your dockerfile (or install it temporarily, if you want less fun)

### 2. In composer
- `composer config repositories.fun-zone composer https://gitlab.com/api/v4/group/fun-zone/-/packages/composer/`
- `composer require fun-zone/motus`

### 3. in codeception
- run tests command with `--ext Fun` parameter

- or in codeception.yml add:
```yml
extensions:
    enabled:
        - Codeception\Extension\Fun
```


